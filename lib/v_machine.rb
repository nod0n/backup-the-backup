# a list of VMachine Resources
# Virtual Machine == VMachine == VM
class VMachine
  def initialize(path)
    @path, @id = path, path.basename.to_s
    @resources = Utils.select_children(@path, Patterns::RESOURCE).map do |child|
      Resource.new(child)
    end
    @disk_image_items = @resources.select do |r|
      r.disk_image_items.any?
    end.map do |r|
      r.disk_image_items
    end.flatten
  end

  attr_reader :path, :id, :disk_image_items

  def prepare_backup
    @disk_image_items.each do |d|
      d.do_hidden_copy
      d.rebase
    end
  end

  def cleanup_hidden_copy
    @disk_image_items.each do |d|
      d.delete_hidden_copy
    end
  end
end
