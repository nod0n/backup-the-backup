# a list of Snapshots
class Workload
  def initialize(path)
    @path, @id = path, path.basename.to_s
    @snapshots = Utils.select_children(@path, Patterns::SNAPSHOT).map do |child|
      Snapshot.new(child)
    end
    @latest_snapshot = @snapshots.max do |a, b|
      a.path.mtime <=> b.path.mtime
    end
    @meta_data = Utils.files(@path.children)
  end

  def backup_latest_snapshot(borg_repo_path)
    backup_name = 'trilio_' + @id + '_' + START_DAY
    data = [@latest_snapshot.path] + @meta_data
    excludes = @latest_snapshot.disk_image_items.map { |d| d.path }

    return if Utils.backup_exist? borg_repo_path, backup_name

    @latest_snapshot.prepare_backup
    Utils.backup_workload(
        borg_repo_path: borg_repo_path,
        data: data, backup_name: backup_name,
        excludes: excludes
    )
    @latest_snapshot.cleanup_backup
    Utils.prune_backups(borg_repo_path, 'trilio_' + @id + '_')
  end
end
