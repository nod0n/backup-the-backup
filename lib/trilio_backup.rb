# a list of Workloads
class TrilioBackup
  def initialize(path)
    @path = path
    @workloads = Utils.select_children(path, Patterns::WORKLOAD).map do |child|
      Workload.new(child)
    end
  end

  def backup_only_latest_snapshots(borg_repo_path)
    Utils.ensure_repo(borg_repo_path)
    @workloads.each { |w| w.backup_latest_snapshot borg_repo_path }
  end
end
