module Patterns
  uuid = '[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}'
  UUID = /#{uuid}/
  WORKLOAD = /^workload_#{uuid}$/
  SNAPSHOT = /^snapshot_#{uuid}$/
  VMACHINE = /^vm_#{uuid}$/
  RESOURCE = /^vm_resource_#{uuid}$/
  ITEM = /^#{uuid}/
  DISK = /^qemu qcow2 image \(v3\)/i
end
