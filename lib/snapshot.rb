# a list of Virtual Machines (VMachine, VM)
class Snapshot
  def initialize(path)
    @path = path
    @vms = Utils.select_children(@path, Patterns::VMACHINE).map do |child|
      VMachine.new(child)
    end
    @disk_image_items = @vms.map do |v|
      v.disk_image_items
    end.flatten
  end

  attr_reader :path, :disk_image_items

  def prepare_backup
    @vms.each { |v| v.prepare_backup }
  end

  def cleanup_backup
    @vms.each { |v| v.cleanup_hidden_copy }
  end
end
