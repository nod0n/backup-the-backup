# Utility Module, kinda reusable
module Utils
  class << self
    def run_command(*args, dry_run: false, verbose: false, exception: true)
      if verbose
        start_time = Time.now
        puts "\nrunning: system(*#{args.flatten})\n\n"
        success = (system(*args.flatten, exception: exception) unless dry_run)
        puts "\nSuccess:\t#{success}\n"
        puts "duration:\t#{(Time.now - start_time).round.to_i}\n\n"
        success
      else
        unless dry_run
          IO.popen(args.flatten) { |o| o.read }
          raise if $?.exitstatus != 0 && exception
        end
      end
    end

    def command_output(*args, dry_run: false, verbose: false, exception: true)
      puts "\nrunning: IO.popen(#{args.flatten})\n\n" if verbose
      unless dry_run
        output = IO.popen(args.flatten) { |o| o.read }
        raise if $?.exitstatus != 0 && exception
        output
      end
    end

    # similar to `find 'path' -mindepth 1 -maxdepth 1 -regex 'pattern'`
    def select_children(path, pattern)
      path.children(false).select do |child|
        child.to_s.match?(pattern)
      end.map do |child|
        path + child
      end
    end

    def files(path)
      [path].flatten!
      path.select { |p| p.file? }
    end

    def file_type(path)
      command_output(%w(file -b) << path.to_s)
    end

    def copy_file(src_path, dest_path, overwrite = false)
      if dest_path.exist?
        return unless overwrite
      end

      run_command(
        %w(rsync --verbose --progress) +
          %w(--owner --group --perms --times --acls --xattrs),
        src_path.to_s, dest_path.to_s,
        verbose: true
      )
    end

    def delete_file(path)
      run_command(['rm', path.to_s], verbose: true)
    end

    def good_image?(path)
      run_command(
        %w(qemu-img check) << path.to_s,
        verbose: true, exception: false
      )
    end

    def rebased_image?(path)
      image_info = JSON.parse(
        command_output(%w(qemu-img info --output=json) << path.to_s)
      )
      !image_info.include?('backing-filename')
    end

    def rebase_image(path)
      Utils.rebased_image?(path) && return

      run_command(
        %w(qemu-img rebase -p -b), '', path.to_s,
        verbose: true
      )
    end

    def ensure_repo(borg_repo_path)
      run_command(
        'borg', 'info', borg_repo_path.to_s,
        verbose: true, exception: false
      ) ||
        run_command(
          %w(borg init --encryption repokey-blake2), borg_repo_path,
          verbose: true
        )
    end

    def backup_workload(
      options = %w(--verbose --progress --one-file-system --compression zstd,4),
      borg_repo_path:, backup_name:, data:, excludes:
    )
      Tempfile.open do |f|
        f << excludes.join("\n")
        f.flush
        run_command(
          'borg', 'create', options,
          '--exclude-from', f.path,
          borg_repo_path.to_s + '::' + backup_name,
          data.map { |d| d.to_s },
          verbose: true
        )
      end
    end

    def backup_exist?(borg_repo_path, backup_name)
      run_command(
        'borg', 'info',
        borg_repo_path.to_s + '::' + backup_name,
        verbose: true, exception: false
      )
    end

    def prune_backups(
      options = %w(--verbose --progress --stats --keep-last 1),
      borg_repo_path, prefix
    )
      run_command(
        'borg', 'prune', options,
        '--prefix', prefix, borg_repo_path.to_s,
        verbose: true
      )
    end
  end
end
