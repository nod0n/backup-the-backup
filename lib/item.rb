# a VM Resource Item: disks, ...?
class Item
  def initialize(path)
    @path = path
    @hidden_copy_path = @path.parent + ('.' + @path.basename.to_s + '_bck')
    @type = case Utils.file_type @path
            when Patterns::DISK
              :image
            else
              :unknown
            end
  end

  attr_reader :path, :type

  def do_hidden_copy
    if @hidden_copy_path.file? &&
        Utils.good_image?(@hidden_copy_path)
      return
    end

    Utils.copy_file(@path, @hidden_copy_path, true)
  end

  def delete_hidden_copy
    Utils.delete_file(@hidden_copy_path)
  end

  def rebase
    Utils.rebase_image(@hidden_copy_path)
  end
end
