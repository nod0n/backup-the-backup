# A list of VM Resource Items
class Resource
  def initialize(path)
    @path = path
    @items = Utils.select_children(@path, Patterns::ITEM).map do |child|
      Item.new(child)
    end
    @disk_image_items = @items.select { |i| i.type == :image }
  end

  attr_reader :disk_image_items
end
