#!/usr/bin/env ruby

# backup trilio vault backup -> external disks, remote SSH, NFS

#noinspection RubyResolve
Encoding.default_internal = Encoding::UTF_8
#noinspection RubyResolve
Encoding.default_external = Encoding::UTF_8

require 'fileutils'
require 'pathname'
require 'json'
require 'date'
require 'tempfile'
require_relative 'lib/item'
require_relative 'lib/patterns'
require_relative 'lib/resource'
require_relative 'lib/snapshot'
require_relative 'lib/trilio_backup'
require_relative 'lib/utils'
require_relative 'lib/v_machine'
require_relative 'lib/workload'

nfs_mount_point = Pathname.new('/srv/pq04_bck1')
borg_repo_path = 'pq04:/share/external/DEV3302_1/borg'
START_DAY = DateTime.now.strftime('%Y%m%d')
ENV['BORG_DISPLAY_PASSPHRASE'] = 'no'

trilio_backup = TrilioBackup.new nfs_mount_point
trilio_backup.backup_only_latest_snapshots borg_repo_path
