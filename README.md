# About

This tool (right now) only works together
with [Trilio Vault](https://www.trilio.io/red-hat-virtualization-2/). This tool
can be used to create offline or offsite backups of the Trilio Vault incremental
backups. This tool adds the possibility to selectively keep older backups as
required.

# Some technical Information

It uses [Borg Backup](https://borgbackup.readthedocs.io/en/stable/index.html)
and benefits from it's deduplication feature. The principle is to run this tool
when you need an offline backup and after that rsync the data produced by this
tool to an external disk. This tool greatly reduces the storage required on the
external disk.

# Why?

* Trilio Vault doesn't offer to selectively keep backups, you can only decide on
  how long you would like each incremental backup or how many incremental
  backups should be kept. You can't for example keep a backup for every day for
  a week, after that keep one backup a week for a month, after that keep a
  backup for every month for a year and after that keep a backup for each year
  for ever.
* Trilio Vault does not have to ability to "export" a backup, to be able to
  create offline backups for disaster recovery (all Servers are destroyed
  including the backups / ransomware encrypts everything including the backups).

# What it does

1. A [Trilio Vault](https://www.trilio.io/red-hat-virtualization-2/)
   incremental backup consists of chained qcow2 files and some meta data. This
   tool creates full backups out of
   incremental [Trilio Vault](https://www.trilio.io/red-hat-virtualization-2/)
   backups by converting chained qcow2 images into a single qcow2 image with
   only the latest data (`qemu-img rebase -p -b ''`). The existing Trilio Vault
   backups are not being modified in any way.
1. The full backups are then stored in
   a [Borg Backup repository](https://borgbackup.readthedocs.io/en/stable/internals/data-structures.html#repository)
   . Compression and deduplication are done by Borg Backup in the process.
1.

The [Borg Backup archive](https://borgbackup.readthedocs.io/en/stable/man_intro.html?highlight=archive)
containing the full backup is checked for consistency.

1. The full backups are then discarded, as they are not required any more.
1. Only the most recent backup of
   a [Trilio Vault workload](https://docs.trilio.io/rhv/user-guide/workloads) is
   kept in
   the [Borg Backup repository](https://borgbackup.readthedocs.io/en/stable/internals/data-structures.html#repository)
   . When this tool finishes,
   the [Borg Backup repository](https://borgbackup.readthedocs.io/en/stable/internals/data-structures.html#repository)
   contains all the
   latest [Trilio Vault](https://www.trilio.io/red-hat-virtualization-2/)
   backups as full backups.
   The [Borg Backup repository](https://borgbackup.readthedocs.io/en/stable/internals/data-structures.html#repository)
   can be rsynced to an external disk and stored in a safe place for later usage
   for example in a disaster case or when data is required that has already been
   purged from the Trilio Vault backup.

# Graph

```mermaid
sequenceDiagram
    loop @workloads.each
    TrilioBackup->>+Workload: backup only latest snapshots
        Workload->>+Latest Snapshot: prepare backup
            loop @vms.each
            Latest Snapshot->>+Virtual Machine: prepare backup
                loop @disk_image_resources.each
                Virtual Machine->>+Disk Image Resource: create hidden copy
                Note over Virtual Machine,Disk Image Resource: rsync
                Disk Image Resource-->>-Virtual Machine: OK
                Virtual Machine->>+Disk Image Resource: make image independent
                Note over Virtual Machine,Disk Image Resource: qemu-img rebase -b ''
                Disk Image Resource-->>-Virtual Machine: OK
                end
            Virtual Machine-->>-Latest Snapshot: OK
            end
        Latest Snapshot-->>-Workload: ok
        Workload->>Workload: backup
        Note over Workload,Workload: borg create ../workload_...
        Workload->>+Latest Snapshot: cleanup
            loop @vms.each
            Latest Snapshot->>+Virtual Machine: cleanup hidden copy
                loop @disk_image_resources.each
                Virtual Machine->>+Disk Image Resource: delete
                Disk Image Resource-->>-Virtual Machine: OK
                end
            Virtual Machine-->>-Latest Snapshot: ok
            end
        Latest Snapshot-->>-Workload: ok
        Workload->>Workload: keep latest backup
        Note over Workload,Workload: borg prune --keep-last 1
    Workload-->>-TrilioBackup: ok
    end
```

# Restore

## Trilio

1. restore data (borg mount + rsync -a / borg extract)
1. fix owner and permissions of the workload directories # TODO what are the correct permissions?
1. rename qcow2 file back to original name '.1234_bck' -> '1234'
1. reinitialize Trilio
1. reconfigure Trilio
1.
    1. reboot Trilio OR
    1.
        1. restart services
            1. wlm-api
            1. wlm-scheduler
            1. wlm-workloads
            1. mysql 1.rabbitmq-server
        1. check ques esist and all ques should have 0 messages (if not, the
           message has not been taken from receiving
           service) `rabbitmqctl list_queues`
            1. workloadmgr-scheduler
            1. workloadmgr-workloads.p-sv-bl-bck01
            1. workloadmgr-scheduler.p-sv-bl-bck01,
            1. workloadmgr-workloads
1. import all workloads
1. restore old snapshot

## By hand (without Trilio)

1. Upload qcow2 files (Disk Image Resource) as Disk Image to RHV
2. Create VMs with the uploaded Disk Images
